#TeacherWait 老師修但幾咧
##Main idea
由於遠距上課大家太安靜，難以掌握大家對於課程的理解狀態。
有問題嗎？一片靜默，事後聽到不少同學其實不懂。
於是建立了這個利用 Line Notify 的方式來達到「匿名傳送訊息」的功能。
##Change log
###Version 1
初步的將Line Notify弄出來，畫面簡易，簡單美化。
###Version 2
由於有時候需要點名，但雖然谷歌有插件可以用，不過尚未達到我想要的效果。
因此做了這個點名系統。
尚未解決的事是可以代點（但大家其實沒那麼勤勞，暫時懶得解決xD）
##Envrioment
用Python 3.6.3 + Django 2.2.6 搭配 ngrok 進行簡易的臨時部署（因為只要開上課時間。）
##Usegae
```./ngrok http 8000```
```python manage.py runserver```