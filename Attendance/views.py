from django.shortcuts import render
from Attendance.forms import RegisterForm
from .models import *
from datetime import datetime
import hashlib
from django.utils import timezone

def register(request):
	if request.method == "POST":
		registerForm = RegisterForm(request.POST)

		errors = []
		if not registerForm.is_valid():
			errors.extend(registerForm.errors.values())
			return render(request, 'Attendance/register.html', locals())

		else:
			sStudent_id = registerForm.cleaned_data['sStudent_id']
			cClass = registerForm.cleaned_data['cClass']
			iSeat_number = registerForm.cleaned_data['iSeat_number']
			sName = registerForm.cleaned_data['sName']
			hashPassword = registerForm.cleaned_data['hashPassword']

		if (len(hashPassword) < 8):
			errors.append('不是跟你說密碼要大於7個字ㄇ😠')
			return render(request, 'Attendance/register.html', locals())

		filterSameUser = Student.objects.filter(sStudent_id = sStudent_id)
		if (len(filterSameUser) > 0):
			errors.append('使用此學號的使用者已存在。')
			return render(request, 'Attendance/register.html', locals())

		student = Student()
		student.sStudent_id = sStudent_id
		student.cClass = cClass
		student.iSeat_number = iSeat_number
		student.sName = sName
		student.hashPassword = hashlib.sha1(hashPassword.encode()).hexdigest()
		student.save()

		message = ["註冊成功（應該啦）",]

	registerForm = RegisterForm(request.POST)
	return render(request, 'Attendance/register.html', locals())

def attendance(request, sTitle):
	if len(Attendance.objects.filter(sTitle = sTitle)) == 0:
		return render(request, 'Attendance/error.html', locals())
	# get Attendance
	attendance_data = Attendance.objects.filter(sTitle = sTitle)[0]
	cClass = attendance_data.cClass
	student_list = attendance_data.student_attendance_set.order_by('sStudent_id__iSeat_number')
	total_num = len(student_list)
	arrived_num = len(student_list.filter(bArrived = True))

	# new create Attendance or someone new sgin up
	if len(student_list) != len(Student.objects.filter(cClass = cClass)):
		for student in (Student.objects.filter(cClass = cClass)):
			obj, created = student_list.get_or_create(
					sStudent_id = student,
					sAttendance = attendance_data,
				)

	# sign in
	success = True
	if request.POST != {}:
		if attendance_data.sDate < timezone.now():
			success = False
			status = '本節課已經結束，無法再簽到！'
			return render(request, 'Attendance/attendance.html', locals())
		else:
			sStudent_id = request.POST['sStudent_id']
			hashPassword = hashlib.sha1(request.POST['hashPassword'].encode()).hexdigest()
			student = Student.objects.filter(sStudent_id = sStudent_id, hashPassword = hashPassword)

			if len(student) != 0:
				student = student[0]
				if student_list.filter(sStudent_id = student)[0].bArrived == True:
					success = False
					status = student.sName + ' 已經簽到過了啦！'
				else:
					student_list.filter(sStudent_id = student).update(bArrived = True, sTime = timezone.now())
					status = student.sName + ' 簽到成功！'

				# get Attendance
				attendance_data = Attendance.objects.filter(sTitle = sTitle)[0]
				cClass = attendance_data.cClass
				student_list = attendance_data.student_attendance_set.order_by('sStudent_id__iSeat_number')
				return render(request, 'Attendance/attendance.html', locals())

			else:
				status = '學號或密碼錯誤。或不是本堂課的同學'
				success = False
				return render(request, 'Attendance/attendance.html', locals())

	return render(request, 'Attendance/attendance.html', locals())

def attendance_list(request):
	attendance_209 = Attendance.objects.filter(cClass = '1').order_by('-sDate')
	attendance_105 = Attendance.objects.filter(cClass = '2').order_by('-sDate')
	attendance_112 = Attendance.objects.filter(cClass = '3').order_by('-sDate')
	return render(request, 'Attendance/attendance_list.html', locals())