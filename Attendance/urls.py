# urls.py
from django.urls import path
from . import views
 
urlpatterns = [
    path('register', views.register),
    path('<str:sTitle>', views.attendance),
    path('', views.attendance_list),
]