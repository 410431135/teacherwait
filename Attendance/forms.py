# form.py
from django import forms
from Attendance.models import *
from datetime import datetime

class RegisterForm(forms.Form):
	CLASSNUMBER_CHOICE = (
		('1', '二年九班'),
		('2', '一年五班'),
		('3', '一年十二班'),
	)
	sStudent_id = forms.CharField(label = "學號", max_length = 16, widget = forms.TextInput(attrs={'id' : 'sStudent_id', 'class':'form-control','placeholder':'輸入學號'}))
	cClass = forms.ChoiceField(label = '班級', choices = CLASSNUMBER_CHOICE, widget = forms.Select(attrs={'id': 'cClass', 'class':'form-control',}))
	iSeat_number = forms.IntegerField(label = "座號", widget = forms.NumberInput(attrs={'id':'iSeat_number', 'class':'form-control',}))
	sName = forms.CharField(label = '姓名', max_length = 16, widget = forms.TextInput(attrs={'class':'form-control',}))
	hashPassword = forms.CharField(label = "密碼（大於7個字）", max_length = 256, widget = forms.PasswordInput(attrs={'id' : 'hashPassword','class':'form-control','placeholder':'輸入密碼'}))
