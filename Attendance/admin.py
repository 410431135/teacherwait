from django.contrib import admin
from .models import *

class StudentInline(admin.TabularInline):
	model = Student_Attendance
	verbose_name = '班級學生'
	
@admin.register(Attendance)
class Attendance(admin.ModelAdmin):
	inlines = [StudentInline,]
	pass

@admin.register(Student)
class Student(admin.ModelAdmin):
	pass


