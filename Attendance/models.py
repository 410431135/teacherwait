from django.db import models

class Student(models.Model):
	CLASSNUMBER_CHOICE = (
		('1', '二年九班'),
		('2', '一年五班'),
		('3', '一年十二班'),
	)
	sStudent_id = models.CharField(verbose_name = '學號', max_length = 16, primary_key = True)
	sName = models.CharField(verbose_name = '姓名', max_length = 16)
	iSeat_number = models.IntegerField(verbose_name = "座號", null = True, blank = True)
	cClass = models.CharField(max_length = 2, verbose_name = "班級", choices = CLASSNUMBER_CHOICE, blank = True, null = True)
	hashPassword = models.CharField(max_length = 64, verbose_name = "密碼", blank=True, null=True)
	def __str__(self):
		cClass = ''
		if self.cClass == '1':
			cClass = '二年九班'
		elif self.cClass == '2':
			cClass = '一年五班'
		elif self.cClass == '3':
			cClass = '一年十二班'
		return cClass+'_'+self.sStudent_id+self.sName

class Attendance(models.Model):
	CLASSNUMBER_CHOICE = (
		('1', '二年九班'),
		('2', '一年五班'),
		('3', '一年十二班'),
	)
	sTitle = models.TextField(verbose_name = "名稱")
	sDate = models.DateTimeField(verbose_name = '上課日期', null = True)
	cClass = models.CharField(max_length = 2, verbose_name = "班級", choices = CLASSNUMBER_CHOICE, blank = True, null = True)
	iNode = models.IntegerField(verbose_name = "節數", null = True, blank = True)
	def __str__(self):
		return self.sTitle

class Student_Attendance(models.Model):
	sStudent_id = models.ForeignKey(Student, verbose_name = '學生', on_delete = models.CASCADE)
	sAttendance = models.ForeignKey(Attendance, verbose_name = '出席表', on_delete = models.CASCADE)
	bArrived = models.BooleanField(verbose_name = '是否出席', default = False)
	sTime = models.DateTimeField(verbose_name = '簽到時間', auto_now = False, blank=True, null=True)