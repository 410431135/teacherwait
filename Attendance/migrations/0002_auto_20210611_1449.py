# Generated by Django 2.2.6 on 2021-06-11 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Attendance', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student_attendance',
            name='sTime',
            field=models.DateTimeField(blank=True, null=True, verbose_name='簽到時間'),
        ),
    ]
